from unittest.mock import Mock

import pytest
from huey.exceptions import HueyException, TaskException

from eveapi.esi.errors import ESIException, ESITimeout
from eveapi.util import get_cache_url, get_retry_result


class TestGetCacheUrl:
    def test_url_without_params(self):
        assert 'example.com?' == get_cache_url('example.com', {})

    def test_url_with_param(self):
        assert 'example.com?region=5' == get_cache_url('example.com', {'region': 5})

    def test_sorted_params(self):
        expected = 'example.com?a=1&b=1&c=1'
        assert expected == get_cache_url('example.com', {'a': 1, 'b': 1, 'c': 1})
        assert expected == get_cache_url('example.com', {'c': 1, 'b': 1, 'a': 1})


class TestGetRetryResult:
    def test_successful_result(self):
        expected = 'some data'
        expected_kwargs = {'blocking': True, 'timeout': 5}

        task = Mock()
        task.get.return_value = expected
        task.task.retries = 2

        assert expected == get_retry_result(task, **expected_kwargs)
        task.get.assert_called_once_with(**expected_kwargs)

    def test_returned_exception(self):
        expected = ESIException()

        task = Mock()
        task.get.return_value = expected
        task.task.retries = 2

        with pytest.raises(ESIException):
            get_retry_result(task)

    def test_esi_timeout_on_huey_exception(self):
        task = Mock()
        task.get.side_effect = HueyException()
        task.task.retries = 2

        with pytest.raises(ESITimeout):
            get_retry_result(task)

    def test_esi_exception_on_task_exception(self):
        expected = 3

        task = Mock()
        task.get.side_effect = TaskException(metadata=ESIException)
        task.task.retries = expected - 1

        with pytest.raises(ESIException):
            get_retry_result(task)
        assert expected == task.get.call_count

    def test_two_exceptions_then_success(self):
        expected = 'some data'
        expected_calls = 3

        task = Mock()
        task.task.retries = expected_calls - 1

        task.get.side_effect = [
            TaskException(metadata=ESIException),
            TaskException(metadata=ESIException),
            expected,
        ]
        assert expected == get_retry_result(task)
        assert expected_calls == task.get.call_count
