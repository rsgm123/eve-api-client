from unittest.mock import Mock, patch

import pytest
from huey.api import Result

from eveapi.esi.client import ESIResponse, ESI, get_auth_token


class TestESIClient:
    def test_user_agent_required(self):
        with pytest.raises(Exception, match='User agent required'):
            ESI()

    def test_default_init(self):
        user_agent = 'user_agent'
        esi = ESI(user_agent=user_agent)

        assert None is esi._endpoint
        assert 'latest' == esi._version
        assert 'tranquility' == esi._datasource
        assert 'GET' == esi._method
        assert None is esi._character
        assert None is esi._auth_callback
        assert user_agent == esi._headers['User-Agent']
        assert 'application/json' == esi._headers['Accept']
        assert 2 == len(esi._headers)

    def test_init_with_kwargs(self):
        user_agent = 'user_agent'
        endpoint = '/universe/system_jumps/'
        version = 'v1'
        method = 'POST'
        character = 'eve-shekel'
        headers = {'test': 'test'}
        auth_callback = lambda: 'test'
        esi = ESI(
            user_agent=user_agent,
            endpoint=endpoint,
            version=version,
            method=method,
            headers=headers,
            character=character,
            auth_callback=auth_callback
        )

        assert user_agent == esi._headers['User-Agent']
        assert endpoint == esi._endpoint
        assert version == esi._version
        assert method == esi._method
        assert character == esi._character
        assert auth_callback == esi._auth_callback
        assert 'test' == esi._headers['test']
        assert 3 == len(esi._headers)

    def test_base_init(self):
        user_agent = 'user_agent'
        endpoint = '/universe/system_jumps/'
        version = 'v1'
        method = 'POST'
        character = 'eve-shekel'
        headers = {'test': 'test'}
        auth_callback = lambda: 'test'
        base_esi = ESI(user_agent=user_agent)
        esi = ESI(  # overwrite esi with new esi
            base=base_esi,
            endpoint=endpoint,
            headers=headers,
            version=version,
            method=method,
            character=character,
            auth_callback=auth_callback
        )

        assert base_esi is not esi
        assert user_agent == esi._headers['User-Agent']
        assert endpoint == esi._endpoint
        assert version == esi._version
        assert method == esi._method
        assert character == esi._character
        assert auth_callback == esi._auth_callback
        assert 'test' == esi._headers['test']
        assert 3 == len(esi._headers)

    def test_str(self):
        esi = ESI(user_agent='user_agent', method='POST', version='v2', endpoint='/universe/system_jumps/')
        assert 'POST https://esi.evetech.net/v2//universe/system_jumps/' == str(esi)

    def test_getattr_invalid(self):
        with pytest.raises(Exception, match='Do not start url pieces with "_"'):
            esi = ESI(user_agent='user_agent')._universe

    def test_getattr_self(self):
        expected = '123'
        esi = ESI(user_agent='user_agent', character=123).self
        assert expected == esi._endpoint

    def test_getattr_method(self):
        expected = 'POST'
        esi = ESI(user_agent='user_agent').post
        assert expected == esi._method

    def test_getattr_version(self):
        expected = 'dev'
        esi = ESI(user_agent='user_agent').dev
        assert expected == esi._version

    def test_getattr_path(self):
        expected = 'universe'
        esi = ESI(user_agent='user_agent').universe
        assert expected == esi._endpoint

    def test_multiple_getattr_path(self):
        expected = 'characters/123/notifications/contacts'
        esi = ESI(user_agent='user_agent', character=123).characters.self.notifications.contacts
        assert expected == esi._endpoint

    def test_getitem(self):
        expected = '456'
        esi = ESI(user_agent='user_agent')[456]
        assert expected == esi._endpoint

    def test_getattr_with_getitem(self):
        expected = 'characters/456/notifications/contacts'
        esi = ESI(user_agent='user_agent').characters[456].notifications.contacts
        assert expected == esi._endpoint

    @patch('eveapi.esi.client.ESIResponse')
    @patch('eveapi.esi.client.huey')
    def test_call(self, huey, esi_response):
        expected = 'characters/456/notifications/contacts'
        expected_kwargs = {
            'method': 'GET',
            'url': 'https://esi.evetech.net/latest/universe/structures',
            'headers': {'User-Agent': 'user_agent', 'Accept': 'application/json'},
            'params': {},
            'data': None,
            'fetch_pages': False,
            'cache': True,
            'auth_callback': None,
            'max_age': None
        }

        ESI(user_agent='user_agent').universe.structures()
        huey.esi.assert_called_once_with(**expected_kwargs)
        esi_response.assert_called_once()

    @patch('eveapi.esi.client.huey')
    def test_call_with_params(self, huey):
        params = {'page': 6, 'other params': 'some other text', 'weird<$#data': 'weird?{}|":~`\\data'}
        expected_kwargs = {
            'method': 'GET',
            'url': 'https://esi.evetech.net/latest/universe/structures',
            'headers': {'User-Agent': 'user_agent', 'Accept': 'application/json'},
            'params': params,
            'data': None,
            'fetch_pages': False,
            'cache': True,
            'auth_callback': None,
            'max_age': None,
        }

        ESI(user_agent='user_agent').universe.structures(**params)
        huey.esi.assert_called_once_with(**expected_kwargs)

    @patch('eveapi.esi.client.partial')
    @patch('eveapi.esi.client.get_auth_token')
    @patch('eveapi.esi.client.huey')
    def test_call_with_auth_callback(self, huey, get_auth_token, partial):
        auth_callback = Mock()
        partial_auth_callback = Mock()

        partial_auth_callback.return_value = 'token'
        partial.return_value = partial_auth_callback

        character = 123
        expected_kwargs = {
            'method': 'GET',
            'url': 'https://esi.evetech.net/latest/universe/structures',
            'headers': {'User-Agent': 'user_agent', 'Accept': 'application/json', 'Authorization': 'token'},
            'params': {},
            'data': None,
            'fetch_pages': False,
            'cache': True,
            'auth_callback': partial_auth_callback,
            'max_age': None,
        }

        ESI(user_agent='user_agent', character=character, auth_callback=auth_callback).universe.structures()
        huey.esi.assert_called_once_with(**expected_kwargs)
        partial.assert_called_once_with(get_auth_token, auth_callback, character)
        partial_auth_callback.assert_called_once()

    @patch('eveapi.esi.client.huey')
    def test_call_with_use_huey(self, huey):
        expected = 'some_data'
        huey.esi.call_local.return_value = expected

        expected_kwargs = {
            'method': 'GET',
            'url': 'https://esi.evetech.net/latest/universe/structures',
            'headers': {'User-Agent': 'user_agent', 'Accept': 'application/json'},
            'params': {},
            'data': None,
            'fetch_pages': False,
            'cache': True,
            'auth_callback': None,
            'max_age': None,
        }

        assert expected == ESI(user_agent='user_agent').universe.structures(use_huey=False)
        huey.esi.call_local.assert_called_once_with(**expected_kwargs)

    @patch('eveapi.esi.client.huey')
    def test_call_with_other_task_params(self, huey):
        data = {'some_data': 'some_other_data'}
        fetch_pages = True
        cache = True
        max_age = 60

        expected_kwargs = {
            'method': 'GET',
            'url': 'https://esi.evetech.net/latest/universe/structures',
            'headers': {'User-Agent': 'user_agent', 'Accept': 'application/json'},
            'params': {},
            'data': data,
            'fetch_pages': fetch_pages,
            'cache': cache,
            'auth_callback': None,
            'max_age': max_age,
        }

        ESI(user_agent='user_agent').universe.structures(
            data=data,
            fetch_pages=fetch_pages,
            cache=cache,
            max_age=max_age
        )
        huey.esi.assert_called_once_with(**expected_kwargs)

    def test_fork(self):
        user_agent = 'user_agent'
        endpoint = '/universe/system_jumps/'
        version = 'v1'
        method = 'POST'
        character = 'eve-shekel'
        headers = {'test': 'test'}
        auth_callback = lambda: 'test'
        base_esi = ESI(user_agent=user_agent)
        esi = base_esi._fork(  # overwrite esi with new esi
            endpoint=endpoint,
            headers=headers,
            version=version,
            method=method,
            character=character,
            auth_callback=auth_callback
        )

        assert base_esi is not esi
        assert user_agent == esi._headers['User-Agent']
        assert endpoint == esi._endpoint
        assert version == esi._version
        assert method == esi._method
        assert character == esi._character
        assert auth_callback == esi._auth_callback
        assert 'test' == esi._headers['test']
        assert 3 == len(esi._headers)


class TestESIResponse:
    def test_init(self):
        expected = Mock()
        esi_response = ESIResponse(expected)

        assert expected == esi_response.esi_task

    def test_get(self):
        expected = 'esi data'
        expected_kwargs = {'blocking': True, 'timeout': 5}

        task = Mock()
        task.get.return_value = expected
        task.task.retries = 3

        esi_response = ESIResponse(task)

        assert expected == esi_response.get(**expected_kwargs)
        task.get.assert_called_once_with(**expected_kwargs)


class TestGetAuthToken:
    def test_get_token(self):
        expected = 'token'
        character = 123
        kwargs = {'other_args': 'other_args'}

        auth_callback = Mock()
        auth_callback.return_value = expected

        assert expected == get_auth_token(auth_callback, character, **kwargs)
        auth_callback.assert_called_once_with(character, **kwargs)

    def test_get_token_huey(self):
        expected = 'token'
        character = 123
        kwargs = {'other_args': 'other_args'}

        task = Mock(Result)
        task.get.return_value = expected
        auth_callback = Mock()
        auth_callback.return_value = task

        assert expected == get_auth_token(auth_callback, character, **kwargs)
        auth_callback.assert_called_once_with(character, **kwargs)
        task.get.assert_called_once_with(blocking=True, timeout=60)
