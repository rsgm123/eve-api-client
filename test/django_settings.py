from unittest.mock import Mock

SECRET_KEY = 'h|MrD`/-lo6|?w>_Sb>?7u1~Mu_Km[D'

REDIS_POOL = Mock()
EVEAPI_HUEY_INSTANCE = Mock()
ESI_RETRY = 60
CLIENT_ID = 'CLIENT_ID'
CLIENT_SECRET = 'CLIENT_SECRET'
