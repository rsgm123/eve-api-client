import logging
import re
import time
from functools import partial
from urllib.parse import quote

from huey.api import Result

from eveapi import huey
from eveapi.util import get_retry_result

logger = logging.getLogger(__name__)

base_url = 'https://esi.evetech.net'
oauth_url = 'https://login.eveonline.com/oauth'
token_url = 'https://login.eveonline.com/oauth/token'
authorize_url = 'https://login.eveonline.com/oauth/authorize'


class ESI:
    # for now, take refresh and access tokens as model objects
    def __init__(self, base=None, endpoint=None, headers=None, method=None, user_agent=None, version=None,
                 character=None, auth_callback=None):

        if headers is None:
            headers = {}  # apparently mutable datatype literals in function parameters are bad mkay

        if base:
            self._endpoint = endpoint or base._endpoint
            self._version = version or base._version
            self._datasource = base._datasource
            self._method = method or base._method
            self._headers = {**base._headers, **headers}
            self._character = character or base._character
            self._auth_callback = auth_callback or base._auth_callback

        else:
            self._endpoint = endpoint
            self._version = version or 'latest'
            self._datasource = 'tranquility'
            self._method = method or 'GET'
            self._character = character
            self._auth_callback = auth_callback
            self._headers = {
                'User-Agent': user_agent,
                'Accept': 'application/json',
                **headers,
            }

            if not user_agent:
                raise Exception('User agent required')

    def __str__(self):
        return '{} {}'.format(self._method, '/'.join([
            base_url,
            self._version,
            self._endpoint or '',  # endpoint may be None
        ]))

    def __getattr__(self, attr):
        """
        Adds to the esi path.

        If attr is an http method name, it will set the method attribute instead.
        Similarly, this sets the version.
        This allows you to reuse esi clients as a base for multiple routes that may need different methods or esi versions.

        Example:
            ```
            client = ESI(authtokens, etc.).v1.fleets[id]
            a = client.v2.members
            b = client.wings
            c = b.post()
            ```
        """
        if attr.startswith('_'):
            logger.error('ESI attr {} starts with _, this should not happen'.format(attr))
            raise Exception('Do not start url pieces with "_"')

        elif attr == 'self' and self._character:  # replace self with auth character id
            attr = self._character

        elif attr in ['get', 'options', 'head', 'post', 'put', 'patch', 'delete']:  # set http method
            return ESI(self, method=attr.upper())

        elif re.match(r'(v\d+|latest|dev|legacy)', attr):  # set esi version instead of adding to the path
            return ESI(self, version=attr)

        url = self._endpoint + '/' + str(attr) if self._endpoint else str(attr)  # endpoint may be None
        return ESI(self, url)

    def __getitem__(self, key):
        return self.__getattr__(str(key))

    def __call__(self, data=None, fetch_pages=False, cache=True, use_huey=True, max_age=None, **kwargs):
        url = '/'.join([
            base_url,
            self._version,
            self._endpoint or '',  # endpoint may be None
        ])

        headers = self._headers.copy()
        auth_callback = None

        # Get auth header here so its not explicitly run on greenlet threads, using up db connections
        # if we want to move this to the ESI huey task, we could cache tokens in redis
        # or just use task level locking
        if self._auth_callback and self._character:
            auth_callback = partial(get_auth_token, self._auth_callback, self._character)
            start_time = time.time()
            headers['Authorization'] = auth_callback()
            logger.info('Got authorization header', extra={
                'auth_header': headers.get('Authorization', 'not set'),
                'character': self._character,
                'duration': time.time() - start_time,
            })

        kwargs_ = {
            'method': self._method,
            'url': url,
            'headers': headers,
            'params': kwargs,
            'data': data,
            'fetch_pages': fetch_pages,
            'cache': cache,
            'auth_callback': auth_callback,
            'max_age': max_age,
        }

        # run the esi call without huey, will default to huey on errors or multiple pages
        if not use_huey:
            return huey.esi.call_local(**kwargs_)

        return ESIResponse(huey.esi(**kwargs_))

    def _fork(self, **kwargs):
        return ESI(base=self, **kwargs)


class ESIResponse:
    esi_task = None

    def __init__(self, task):
        self.esi_task = task

    def get(self, **kwargs):
        return get_retry_result(self.esi_task, **kwargs)


def get_auth_token(auth_callback, character, **kwargs):
    auth = auth_callback(character, **kwargs)
    if isinstance(auth, Result):
        auth = auth.get(blocking=True, timeout=60)

    return auth
