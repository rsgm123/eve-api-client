class ESIException(Exception):
    error = None
    metadata = None


class ESIErrorLimitReached(ESIException):
    error = 'ESI error limit hit, please wait'

    def __init__(self, remaining_time):
        self.remaining_time = remaining_time


class ESIPermissionRevoked(ESIException):
    error = 'Character refresh token was revoked'


class ESIPageCacheRefresh(ESIException):
    error = "Current page expiration does not match the first page's expiration, retrying original request"


class ESIMaxAgeExceeded(ESIException):
    error = "ESI returned data that was older than the max age"


class ESIInvalidToken(ESIException):
    error = "Current access token is expired or otherwise invalid, refresh token"


class ESITimeout(ESIException):
    error = "Timed out waiting for ESI task response"
