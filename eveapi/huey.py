import datetime
import json
import logging
from json.decoder import JSONDecodeError

import redis
import requests
from django.conf import settings
from django.utils import timezone

from eveapi.esi.errors import ESIPageCacheRefresh, ESIMaxAgeExceeded, ESIException, ESIInvalidToken
from eveapi.util import get_cache_url, get_retry_result

TIMEOUT = 30
ESI_LOCK = 'esi_api_client.lock'

SESSION = requests.Session()
ADAPTER = requests.adapters.HTTPAdapter(pool_connections=1000, pool_maxsize=1000, max_retries=3)
SESSION.mount('https://', ADAPTER)

logger = logging.getLogger(__name__)
redis = redis.StrictRedis(connection_pool=settings.REDIS_POOL)

HUEY = settings.EVEAPI_HUEY_INSTANCE


def log_data_age(r):
    """
    Logs data age and expiry time in seconds.
    Age is time since last modified, expiry is the time until the esi refreshes the cache.
    """
    age = -1
    expires = -1
    if 'Last-Modified' in r.headers:
        age = datetime.datetime.now() \
              - datetime.datetime.strptime(r.headers['Last-Modified'], '%a, %d %b %Y %H:%M:%S %Z')
        age = round(age.total_seconds())

    if 'Expires' in r.headers:
        expires = datetime.datetime.strptime(r.headers['Expires'], '%a, %d %b %Y %H:%M:%S %Z') - datetime.datetime.now()
        expires = round(expires.total_seconds())

    logger.debug('ESI data is {} seconds old, new ESI data in {} seconds'.format(age, expires),
                 extra={'esi_type': 'data_age', 'age': age, 'expires': expires})

    return age, expires


def error_limit_retry(delay=settings.ESI_RETRY, **kwargs):
    """
    Schedule another esi task after the esi error limit reset ends then return the result.
    """
    return esi.schedule(  # todo: figure out datastore error handling
        kwargs=kwargs,
        delay=delay,
    ).get(timeout=300, blocking=True)


def cache_response(endpoint, expires, data):
    if expires:
        cached_until = timezone.make_aware(datetime.datetime.strptime(expires, '%a, %d %b %Y %H:%M:%S %Z'))
        time_delta = cached_until - timezone.now()
        seconds = int(time_delta.total_seconds())
    else:
        seconds = 300  # default to 5 minutes

    if seconds > 0:
        data = json.dumps(data)
        redis.setex(endpoint, seconds, data)

        return True


@HUEY.task(retries=2, retry_delay=10)
def esi(method, url, headers, params, data, fetch_pages, cache, page=None, parent_expires=None, auth_callback=None,
        max_age=None):
    """
    ESI call method.
    This calls the esi, aggregates pages, and caches results.
    On errors this function will be rerun
    """

    def kwargs_with(**kwargs_):
        return {
            'method': method,
            'url': url,
            'headers': headers,
            'params': params,
            'data': data,
            'fetch_pages': fetch_pages,
            'cache': cache,
            'page': page,
            'parent_expires': parent_expires,
            'auth_callback': auth_callback,
            'max_age': max_age,
            **kwargs_,
        }

    # set page number
    if page:
        params['page'] = page  # unspecified gives the first page in esi

    cache_url = get_cache_url(url, params)

    # check for cached request
    cached = redis.get(cache_url)
    if method == 'GET' and cached:  # return cached get response
        logger.info('Cached ESI request', extra={'esi_type': 'cached_request', 'url': cache_url})
        return json.loads(cached.decode('utf-8'))

    # check for esi_api_client.lock to esi rate limit
    delay = redis.ttl(ESI_LOCK)
    if delay and delay > 0:
        message = redis.get(ESI_LOCK)
        logger.warning('esi locked', extra={'esi_type': 'error_limit_hit', 'delay': delay, 'lock_message': message})
        return error_limit_retry(**kwargs_with(delay=delay + 5))

    # make request
    r = SESSION.request(method, url, params=params, headers=headers, data=data, timeout=TIMEOUT)
    logger.debug('got response in {} seconds'.format(r.elapsed.seconds),
                 extra={'esi_type': 'esi_response', 'duration': r.elapsed.seconds})

    # check ESI error rate limiting
    if 'X-Esi-Error-Limit-Remain' in r.headers and int(r.headers['X-Esi-Error-Limit-Remain']) <= 20:
        delay = r.headers.get('X-Esi-Error-Limit-Reset', settings.ESI_RETRY)
        redis.setex(ESI_LOCK, delay, 'Low error limit remaining, locking for {}'.format(delay))

    if 200 <= r.status_code < 300:  # is 2xx
        age, expires = log_data_age(r)
        logger.info('esi 2xx response', extra={
            'esi_type': '2xx_response',
            'status': r.status_code,
            'response_headers': r.headers,
            'fetch_pages': fetch_pages,
            'age': age,
            'expires': expires,
            'url': cache_url,
            'duration': r.elapsed.seconds
        })

        if max_age and age > max_age:
            return ESIMaxAgeExceeded()  # return exception to quickly fail

        if 'application/json' not in r.headers.get('Content-Type', ''):
            return True

        if parent_expires and r.headers.get('Expires', None) != parent_expires:
            return ESIPageCacheRefresh()  # return exception to quickly retry the parent page request

        try:
            response_data = r.json()
        except JSONDecodeError as ex:
            logger.error('Error reading ESI json data', extra={'esi_type': 'json_decode_error', 'content': r.content})
            raise ex

        # load multiple pages if able to
        total_pages = int(r.headers.get('x-pages', 1))
        if fetch_pages and total_pages > 1:
            logger.debug('got {} pages, fetching all'.format(total_pages),
                         extra={'esi_type': 'fetching_pages', 'pages': total_pages})
            pages = [
                esi(**kwargs_with(
                    fetch_pages=False,
                    page=p + 2,
                    parent_expires=r.headers.get('Expires', None)
                ))
                for p in range(total_pages - 1)  # +2 since pages are 1-based indexed, and we already have page 1
            ]
            for page_request in pages:
                try:
                    page_data = get_retry_result(page_request, blocking=True, timeout=30)  # block to get result
                except ESIException as ex:
                    logger.warning(ex.error, extra={
                        'esi_type': 'fetch_page_error',
                        'page_count': total_pages
                    })
                    for p in pages:
                        p.revoke()

                    # retry this task
                    raise ex  # retry task

                response_data.extend(page_data)  # assume both are lists

        # cache response if get request
        if method == 'GET' and cache:  # assume fetch_pages and save the full data list for page=false
            cache_response(get_cache_url(url, params), r.headers.get('Expires', None), response_data)

        return response_data

    else:
        try:
            data = r.json()
        except JSONDecodeError:
            data = {}

        error_limit = r.headers.get('X-ESI-Error-Limit-Remain', 100)
        error_message = data.get('error', '')
        sso_status = data.get('sso_status', '')

        logger.error('ESI request error', extra={
            'esi_type': 'request_error',
            'url': cache_url,
            'status': r.status_code,
            'auth_header': r.request.headers.get('Authorization', 'not set'),
            'request_headers': r.request.headers,
            'esi_error_content': r.content,
            'esi_error_message': error_message,
            'sso_status': sso_status,
            'error_limit': error_limit,
            'duration': r.elapsed.seconds
        })

        # there was an error refreshing the token, so refresh the token again
        if error_message == 'invalid character \'<\' looking for beginning of value':
            logger.error('Token invalid received, forcing token refresh', extra={'esi_type': 'invalid_token'})
            auth_callback(force=True)  # can raise an Exception if the auth refresh for the token is locked
            return ESIInvalidToken()

        # the token has expired
        # "unexpected end of json" also means expired token sometimes because ccp
        if error_message == 'token is expired' or error_message == 'unexpected end of JSON input':
            logger.error('Token expired', extra={'esi_type': 'invalid_token'})
            return ESIInvalidToken()

        # retry request in 5 minutes after TQ is back up
        if error_message == 'The datasource tranquility is temporarily unavailable':
            redis.setex(ESI_LOCK, 60, 'Tranquility downtime, locking ESI for 60 seconds')
            logger.error('Tranquility downtime, locking ESI for 60 seconds', extra={'esi_type': 'invalid_token'})

        # log error for esi rate limiting
        if error_limit == 0:
            logger.error('ESI error limit hit 0', extra={'esi_type': 'error_limit_hit_0'})

        # don't try to time esi errors to reschedule tasks
        # let the tasks retry and either hit the ESI lock,
        # or quickly fail for the caller to retry if important, freeing up resources
        r.raise_for_status()
