import logging
import math
import time
from urllib.parse import urlencode

from huey.exceptions import HueyException, TaskException

from eveapi.esi.errors import ESIException, ESITimeout

logger = logging.getLogger(__name__)


def get_cache_url(url, params):
    """
    Appends the parameters to the url.
    This is used to cache urls with specific parameters.
    Parameters are sorted by key names since parameters are unordered.

    This may not generate valid urls, this should only be used for redis keys.
    """
    return url + '?' + urlencode(sorted(params.items()))


def get_retry_result(task, **kwargs):
    result = None
    retry_count = task.task.retries

    def raise_or_reset(i, ex):
        if i == retry_count:
            raise ex
        else:
            task.reset()

    # in huey 2, errors are given as results, so we need to reset the result until the last retry
    for i in range(retry_count + 1):
        start_time = time.time()

        try:
            result = task.get(**kwargs)
        except TaskException as ex:  # task raised exception
            logging.error('ESI task failed with exception', extra={'esi_type': 'exception', 'exception': ex.metadata})
            raise_or_reset(i, ESIException())  # might not be an ESIException
        except HueyException as ex:  # probably timeout
            logging.error('Timeout waiting for task results', extra={'esi_type': 'timeout', 'exception': ex})
            raise_or_reset(i, ESITimeout())
        else:  # no exception occurred, return result
            break

        # make sure we stick to the timeout
        if 'timeout' in kwargs:
            kwargs['timeout'] -= math.floor(time.time() - start_time)

    # check for returned exceptions so we can quickly fail the task
    # this will only every return ESIExceptions
    if isinstance(result, ESIException):
        raise result

    return result
